
public class ProductCoinMapper {
	VendorMachineInf vendorMachine;
	public ProductCoinMapper(VendorMachineInf vendorMachine2) {
		this.vendorMachine = vendorMachine2;
		setCoinProductMap();
	}

	void setCoinProductMap(){
		
        this.vendorMachine.setProductCoinList(new OneRupee().getValue(),new Tea());
        this.vendorMachine.setProductCoinList(new TwoRupeeCoin().getValue(),new Coffee());
        this.vendorMachine.setProductCoinList(new ThirdRupeeCoin().getValue(), new Horlicks());
       
	}
	
	public VendorMachineInf getVendorMachine(){
		return this.vendorMachine;
	}

}
