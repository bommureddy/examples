
public class VendorMachineExecutor {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		VendorMachineInf vendorMachine = new VendorMachine();
		ProductCoinMapper mapper = new ProductCoinMapper(vendorMachine);
		vendorMachine = mapper.getVendorMachine();
		vendorMachine.setCoin(new OneRupee());
		Product product = vendorMachine.getProduct();
		System.out.println(product.getType());

	}

}
