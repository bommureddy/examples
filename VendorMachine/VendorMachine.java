import java.util.HashMap;


public class VendorMachine implements VendorMachineInf{
	
	Coin coin;
	Product product;
	private HashMap<Integer,Product> ProductSpecificCoinList = new HashMap<Integer,Product>();
   
	@Override
	public void setCoin(Coin coin) {
		
		this.coin = coin;
		
	}

	@Override
	public Coin getCoin() {
		// TODO Auto-generated method stub
		return coin;
		
	}
	
	public void setProductCoinList(Integer coin,Product product){
		this.ProductSpecificCoinList.put(coin,product);
	}

	@Override
	public Product getProduct() {
		Coin coin = getCoin();
		
		this.product = ProductSpecificCoinList.get(coin.getvalue());
		return product;
	}

}
