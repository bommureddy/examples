
public class ExecutableObserverClass {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		WeatherData wd = new WeatherData();
		CurrentConditionDisplay ccd = new CurrentConditionDisplay();
		StatisticsClassDisplay  scd = new StatisticsClassDisplay();
		wd.addObserver(scd);
		wd.addObserver(ccd);
		wd.setMeasurements(100, 200, 300);

	}

}
