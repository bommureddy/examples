
public interface WeatherDataObservable {
	abstract void addObserver(Observer obs);
	abstract void removeObserver(Observer obs);
	abstract void notifyObservers(Object obj);

}
