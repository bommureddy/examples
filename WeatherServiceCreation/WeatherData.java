
import java.util.ArrayList;
import java.util.Observable;
public class WeatherData implements WeatherDataObservable  {
 private int temp;
 private int humidity;
 private int pressure;
private boolean unchange =false;
private ArrayList<Observer> observerList = new ArrayList<Observer>();

public  int getTemperature(){
	return this.temp;
	
}
public int getHumidity(){
	return this.humidity;
}

public int getPressure(){
	return this.pressure;
}



public void measurementsChanged(){
	int temp = getTemperature();
	int humidity = getHumidity();
	int pressure = getPressure();
	ArrayList<Integer> al = new ArrayList<Integer>();
	al.add(temp);
	al.add(humidity);
	al.add(pressure);
	setChanged();
	notifyObservers((Object)al);
	
}

private void setChanged() {
  unchange  = true;
}
public void setMeasurements(int temp,int pressure,int humidity){
	if((this.temp!=temp || this.pressure!=pressure || this.humidity != humidity)){
	this.temp = temp;
	this.humidity = humidity;
	this.pressure = pressure;
	setChanged();
	measurementsChanged();
	}
}
@Override
public void addObserver(Observer obs) {
	observerList.add(obs);
	
}
@Override
public void removeObserver(Observer obs) {
	observerList.remove(obs);
	
}
@Override
public void notifyObservers(Object obj) {
	if(!unchange)
		return;
	else
	{
       for (Observer obs : observerList){
    	   obs.update(this, obj);
       }
	}
	
}

}
