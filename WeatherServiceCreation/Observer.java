
public interface Observer {
	abstract void update(WeatherDataObservable obs, Object obj);

}
