import java.util.HashMap;
import java.util.Map;


public interface OrderSlip {

	Map<? extends Integer, ? extends String> getOrderList();
	abstract void setOrderList(HashMap<Integer,String> map);
}
