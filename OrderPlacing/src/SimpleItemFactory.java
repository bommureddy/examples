
public class SimpleItemFactory {

	public static Product getProduct(String item) {
		if(item.equals("chickencurry"))
		return new ChickenCurry();
		else if(item.equals("muttoncurry"))
         return new MuttonCurry();
		return null;
	}

}
