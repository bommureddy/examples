
public class Customer {
	
   	String customerName;
   	Waiter waiter ;
   	OrderSlip slip;
   	Customer(String customerName){
   		this.customerName = customerName;
   		System.out.println(customerName+"ready to place  an order" );
   	}
   	
   	public void setOrderTo(Waiter waiter){
   		this.waiter = waiter;
   		System.out.println("Customer has approaced to a waiter");
   	}
   	
   	public void placeOrder(OrderSlip slip){
   		System.out.println("Customer has placed order  to a waiter");
   		waiter.orderUp(slip);
   		
   	}

}
