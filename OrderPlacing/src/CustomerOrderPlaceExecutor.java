import java.util.HashMap;


public class CustomerOrderPlaceExecutor {


	public static void main(String[] args) {
		Customer cust = new Customer("chandrahas");
		HashMap <Integer,String> map = new HashMap<Integer,String>();
		map.put(1, "chickencurry-2");
		map.put(2, "muttoncurry-2");
		OrderSlip slip = new OrderSlipNote();
		slip.setOrderList(map);
		Cheff cheff = new SimpleCheff();
		Waiter waiter = new SimpleWaiter(cheff);
		cust.setOrderTo(waiter);
        cust.placeOrder(slip);
	}

}
