import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


public class SimpleCheff implements Cheff {

	@Override
	public void prepareOrder(OrderSlip slip) {
		System.out.println("Cheff started preparing order");
		createItemsinOrder(slip);
	}

	private void createItemsinOrder(OrderSlip slip) {
		int result =0;
		Map<Integer,String> map = new HashMap<Integer,String>();
		map.putAll(slip.getOrderList());
		for(Entry <Integer,String> entry : map.entrySet()){
			int price =0;
			String[] Item = entry.getValue().split("-");
		  price=createItem(Item);
			result+= price;
		}
	   System.out.println("cheff has prepared order and the total amount is :"+result);
	}

	private int  createItem(String[] item) {
		int cost=0;
		for(int i=0;i<Integer.parseInt(item[1]);i++){
			Product product= SimpleItemFactory.getProduct(item[0]);
			cost+= product.getPrice();
		}
		return cost;
	}

}
