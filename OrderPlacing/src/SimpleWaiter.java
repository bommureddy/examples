
public class SimpleWaiter implements Waiter {
    Cheff cheff;
    
    SimpleWaiter(Cheff cheff){
    	this.cheff = cheff;
    }
	@Override
	public void orderUp(OrderSlip slip) {
		System.out.println("waiter placed order to cheff");
		cheff.prepareOrder(slip);

	}

}
